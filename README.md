This Android app is basically one empty activity that contains a timer which polls some URL.

This URL will output if something was send to it.

In this example the URL to be read from is: `http://cppse.nl/recv.php`.

The implementation for this script could be:


    root@CppSe:/root> cat /srv/www/vhosts/cppse.nl/recv.php 
    <?php
    if (@file_exists('/tmp/recv.txt')) {
        print file_get_contents('/tmp/recv.txt');
        unlink('/tmp/recv.txt');
    }
    ?>


The sending equivalent could be:

    root@CppSe:/root> cat /srv/www/vhosts/cppse.nl/send.php 
    <?php
        file_put_contents('/tmp/recv.txt', urldecode($_SERVER['QUERY_STRING']));
    ?>
    
Wrap that up in a sendmsg script:

    [trigen@localhost ~]$ cat ~/.bin/sendmsg 
    #!/bin/bash
    
    curl -G --data-urlencode "$*" http://cppse.nl/send.php

Use like:

    sendmsg hello world

