package nl.cppse.refocus;

import android.app.Notification;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

public class TriggerNotification {
    Context context = null;
    String msg = "";
    Resources res;
    public TriggerNotification(Context ctx, Resources resources, String message) {
        msg = message;
        context = ctx;
        res = resources;
    }
    public void trigger()
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeResource(res, R.mipmap.ic_launcher, options);
        NotificationCompat.WearableExtender wearableExtender =
                new NotificationCompat.WearableExtender()
                        .setHintHideIcon(true)
                        .setBackground(bmp);
        Notification notif = new NotificationCompat.Builder(context)
                .setContentTitle("REFOCUS")
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setVibrate(new long[]{300, 300, 300, 300, 300, 300})
                .setLights(Color.RED, 3000, 3000)
                .extend(wearableExtender)
                .build();
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(context);
        notificationManager.notify(12345, notif);
    }
}
