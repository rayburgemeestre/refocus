package nl.cppse.refocus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        final Button toggleServiceButton = (Button) findViewById(R.id.buttonToggleService);
        final Button testNotificationButton = (Button) findViewById(R.id.buttonTestNotification);
        final Intent intent = new Intent(getBaseContext(), NotificationsChecker.class);
        intent.putExtra("PeerId", "TEST");

        final EditText outputText = (EditText) findViewById(R.id.outputtext);
        outputText.append("Initialized\n");

        //intent.putExtra("OutputText", outputText);

        toggleServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.textviewServiceRunning);
                if (toggleServiceButton.getText().equals("STOP SERVICE")) {
                    stopService(intent);
                    tv.setText("stopped");
                    outputText.append("Stopping service in background..\n");
                    toggleServiceButton.setText("START SERVICE");
                } else {
                    startService(intent);
                    tv.setText("running");
                    outputText.append("Starting service in background..\n");
                    toggleServiceButton.setText("STOP SERVICE");
                }
            }
        });

        testNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TriggerNotification tn = new TriggerNotification(getApplicationContext(), getResources(), "Hello world!");
                tn.trigger();
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra(NotificationsChecker.NOTIF_MSG);
                outputText.append(s + "\n");
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(NotificationsChecker.NOTIF_RES)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }
}
