package nl.cppse.refocus;

import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;


public class NotificationsChecker extends Service {
    private String peer_id;
    private LocalBroadcastManager broadcaster;

    public NotificationsChecker() {
    }

    static final public String NOTIF_RES = "nl.cppse.refocus.backend.NotificationsChecker.REQUEST_PROCESSED";
    static final public String NOTIF_MSG = "nl.cppse.refocus.backend.NotificationsChecker.NOTIFICATION_MSG";

    public void sendResult(String message) {
        Intent intent = new Intent(NOTIF_RES);
        if(message != null)
            intent.putExtra(NOTIF_MSG, message);
        broadcaster.sendBroadcast(intent);
    }

    @Override
    public void onCreate()
    {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        peer_id = (String) intent.getExtras().get("PeerId");
        Log.d("NotificationsChecker", "peer_id = " + peer_id);
        return null;
    }

    Timer timer;

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        peer_id = (String) intent.getExtras().get("PeerId");
        Log.d("NotificationsChecker", "peer_id = " + peer_id);



        final NotificationsChecker self = this;

        int delay = 5000; // delay for 5 sec.
        int period = 5000; // repeat every sec.

        timer = new Timer();

        try {
            final URL url = new URL("http://cppse.nl/recv.php");
            //final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setRequestProperty("Connection","Keep-Alive");

            timer.scheduleAtFixedRate(new TimerTask()
            {
                public void run() {

                    try {
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestProperty("Connection","Keep-Alive");

                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        StringBuilder sb = new StringBuilder();
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String read;

                        while ((read = br.readLine()) != null) {
                            sb.append(read);
                        }

                        br.close();
                        String msg = sb.toString();
                        if (msg.trim().isEmpty())
                            return;

                        TriggerNotification tn = new TriggerNotification(getApplicationContext(), getResources(), msg);
                        tn.trigger();

                        sendResult("Notification received: " + msg);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, delay, period);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        sendResult("Shutting down service");

        timer.cancel();
        timer.purge();

        super.onDestroy();
    }
}
